package com.hcl;
import java .util .Scanner;

public class Calculator {

		public static void main(String[] args) {

		    Scanner obj = new Scanner(System.in);

		    System.out.println("Enter numbers: ");
		    double Value1 = obj.nextDouble();
		    double Value2 = obj.nextDouble();
		    
		    System.out.println("Select operator: +,-,*or/");
		    char operator = obj.next().charAt(0);
		    
		    double Result = 0;
		    
		    switch(operator) {

		      case '+':
		        Result = Value1+Value2;
		        break;
		      case '-':
		        Result = Value1-Value2;
		        break;
		      case '*':
		        Result = Value1*Value2;
		        break;
		      case '/':
		    	  if(Value2==0) {
				    	System.out.println("Can't be divided");
		    	  }
		    	  else {
		    		  Result = Value1/Value2;
		    	  }
		    	  break;

		      default:
		        System.out.println("Irrelevant Operator");
		        break;
		    }
		    System.out.println(Value1+""+operator+""+Value2+"="+Result);



}
}
