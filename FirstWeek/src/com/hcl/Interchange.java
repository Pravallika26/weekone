package com.hcl;
import java.util.Scanner;

public class Interchange {

	public static void main(String[] args) {
		
		Scanner obj = new Scanner(System.in);
		System.out.println("Enter numbers: ");
		int val1 = obj.nextInt();
		int val2 = obj.nextInt();
		
		System.out.println("Before interchanging: "+val1+"  "+val2);
		
		val1=val1+val2;
		val2=val1-val2;
		val1=val1-val2;
		
		System.out.println("After interchanging: "+val1+"  "+val2);
		

	}

}
