package com.hcl;

import java.util.Scanner;

class ReverseFinder {

	/*static int findReverse(int number) {
		return number;

	}*/

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
        System.out.println("Enter Number: ");
		int number = in.nextInt(); 
		int reverse =0;
		
		while(number!=0) {
			int rem = number%10;
			reverse = reverse*10+rem;
			number = number/10;
		}

		System.out.println(reverse);

	}

}
