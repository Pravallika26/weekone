package com.hcl;

import java.util.Scanner;

public class PatternPrinting {

	//static void printPattern(int num) {

	//}

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
        System.out.println("No of rows: ");
		int num = in.nextInt();
		
		int initial =1;
		for(int i=1;i<=num;i++) {
			for(int j=1;j<=i;j++) {
				System.out.print(initial++ +" ") ;
			}
			System.out.println();
		}

	}

}
