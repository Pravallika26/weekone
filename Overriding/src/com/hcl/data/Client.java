package com.hcl.data;

public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Employee e1 = new Employee();
		e1.setId(1);
		e1.setName("ram");
		e1.setSalary(30000.45);
		System.out.println(e1); 
		
		Employee e2 = new Employee();	
		e2.setId(1);
		e2.setName("ram");
		e2.setSalary(50000.66);
		System.out.println(e2); 
		
		System.out.println(e1==e2);
		System.out.println(e1.equals(e2));
		System.out.println(e1.hashCode());
	}

}
