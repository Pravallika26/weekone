package com.hcl.data;

public class Employee {

	private int id;
	private String name;
	private double salary;

	public Employee() {
		super();

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String toString() {
		return "Employee[Id:" + id + ",Name:" + name + ",Salary:" + salary + "]";
	}

	public boolean equals(Object obj) {

		boolean val = false;
		Employee e = (Employee) obj;
		if (this.id == e.id) {
			val = true;
		}
		return val;
	}

	public int hashCode() {		
		return getId();
}
}