package com.hcl.strings;

public class StringArrayDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String name = new String("Pravallika  ");
		String name1 = "Gannavarapu";
		
		System.out.println(name.concat(name1));
		System.out.println(name.toUpperCase());
		System.out.println(name.charAt(3));
		System.out.println(name.length());
		System.out.println(name.equals(name1));
		System.out.println(name == name1);
		
		String s1 = "praval";
		String s2 = "praval";
		System.out.println(s1.equals(s2));
		System.out.println(s1 == s2);
		
		String s3 = new String("java");
		String s4 = new String("java");
		System.out.println(s3.equals(s4));
		System.out.println(s3 == s4);
		
		int arr[] = new int[3];
		System.out.println(arr);
		
		int arr1[] = {12,34,3};
		System.out.println(arr1);
		
		System.out.println(arr[0]);
		System.out.println(arr1[2]);
	    arr[0]=1;
	    arr[1]=2;
	    arr[2]=3;
	    System.out.println(arr[0]);
	    System.out.println(arr[1]);
	    System.out.println(arr[2]);
		
		String str = "sai";
		System.out.println(str);
		

	}

}
