package com.hcl.controls;

public class ControlDemo {

	public static void main(String[] args) {
		
		int num1 = 12;
		int num2 = 24;

		if (num1 > num2) {
			System.out.println("num1 is big");
		} 
		else {
			System.out.println("num2 is big");
		}
		
		
		int value = 1;
		switch (value) {
		case 4:
			System.out.println("first");
			break;
		case 8:
			System.out.println("second");
			break;
		case 12:
			System.out.println("third");
			break;
		default:
			System.out.println("none");
			break;
		}

	}

}