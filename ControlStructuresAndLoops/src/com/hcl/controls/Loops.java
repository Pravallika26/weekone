package com.hcl.controls;

public class Loops {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		for(int i=0;i<10;i++) {
			System.out.println("hi "+i);
		}
		
		int value = 5;
		while(value!=0) {
			System.out.println("Hello "+value);
			value--;
		}
		
		int val = 1;
		do {
			System.out.println("Welcome "+val);
			val++;
		} while (val>=10);
		
		
		int arr[] = {12,17,24,46,60};
		for (int amount : arr) {
			System.out.println(amount);
		}
		
		}
		

	}
