package com.hcl.variable;

public class DataTypesDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		byte value = 5;
		short amount = 100;
		int number = 150;
		long bill = 1500l;
		float meters = 1.5f;
		double num = 55.555d;
		char name = 'a';
		boolean message = true;
		System.out.println("Number :" + number);
		System.out.println("Byte " + value);
		System.out.println("Short " + amount);
		System.out.println("long " + bill);
		System.out.println("float " + meters);
		System.out.println("double "+num);
		System.out.println("char "+name);
		System.out.println("bool "+message);

	}

}
