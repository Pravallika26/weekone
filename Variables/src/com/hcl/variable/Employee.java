package com.hcl.variable;

public class Employee {
	
    private int eid;
	private String name;
	
	public Employee() {
		
	}
	
	public Employee(int eid,String name) {
		this.eid = eid;
		this.name = name;
		
	}
	public boolean equals(Object obj) {
		boolean flag = false;
		Employee emp = (Employee) obj;
		if(this.eid == emp.eid) {
			flag = true;
		}
		return flag;
	}
	public void setEid(int eid) {
		this.eid = eid;
	}
		public int getEid() {
			return this.eid;
		}
	public void setName(String name) {
			this.name = name;
	}
			public String getName() {
				return name;


			}			
}
