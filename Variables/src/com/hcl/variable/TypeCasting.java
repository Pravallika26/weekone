package com.hcl.variable;

public class TypeCasting {

	public static void main(String[] args) {

		byte value = 5;
		int num = value;
		System.out.println("implicit " + num);

		int meters = 15;
		double amount = 75.55;
	    meters = (int) amount;
		System.out.println("explicit "+meters);
		
		long input = 150;
		float input1 = input;
		System.out.println("special "+input1);

	}

}
